/*
  WiFi Web Server

 Change the macro WIFI_AP, WIFI_PASSWORD and WIFI_AUTH to your own router settings.

 */
#include <Wire.h>
#include <rgb_lcd.h>

#include <LTask.h>
#include <LWiFi.h>
#include <LWiFiServer.h>
#include <LWiFiClient.h>

#define WIFI_AP "LinkitOneWifi"
#define WIFI_PASSWORD ""
#define WIFI_AUTH LWIFI_OPEN  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP according to your WiFi AP configuration
//I would try LWIFI_WPA first
//if that fails LWIFI_WEP
//if that fails LWIFI_OPEN (and find out why you are giving WiFi to the neighbours!)

int serverPort = 80;
LWiFiServer server(serverPort);

rgb_lcd lcd;
const int colorR = 255;
const int colorG = 0;
const int colorB = 0;

void setup()
{
  lcd.begin(16,2);
  lcd.setRGB(colorR, colorG, colorB);
  LWiFi.begin();
  Serial.begin(115200);
  // keep retrying until connected to AP
  Serial.println("Connecting to AP");
  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
  {
    lcd.setCursor(0,1);
    lcd.print("Search");
  }
  printWifiStatus();
  Serial.println("Start Server");
  server.begin();
  Serial.println("Server Started");
  lcd.setCursor(0,0);
  lcd.print("Bienvenue");
}

int loopCount = 0;

void loop()
{
  // put your main code here, to run repeatedly:
  String str = "";
  String url = "";
  int i;
  delay(500);
  loopCount++;
  LWiFiClient client = server.available();
  if (client)
  {
    Serial.println("new client");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    while (client.connected())
    {
      if (client.available())
      {
        // we basically ignores client request, but wait for HTTP request end
        char c = client.read();
        Serial.print(c);
        if(c != '\n')
          str += c;
        if(c == '\n')
        {
          //Serial.println(str);
          if(str.startsWith("GET "))
          {
            url = str.substring(4, str.lastIndexOf(" "));
            Serial.print("URL:");
            Serial.print(url);
            Serial.println(":");
          }
          str = "";
        }

        if (c == '\n' && currentLineIsBlank)
        {
          Serial.println("send response");
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
          client.println();
          if(url != String("favicon.ico"))
          {
            client.println("<!DOCTYPE HTML>");
            client.println("<html>\n<head>\n<title>Qui prend</title>\n</head>");
            client.println("<body>");
            //i = digitalRead(LED);
            url.toLowerCase();
            if(url == String("/eleve"))
            {
               lcd.setCursor(0,1);
               lcd.print("Eleve a la clé");
              client.println("un élève possède la clé<br>");
            }
            else if(url == String("/prof"))
            {
              lcd.setCursor(0,1);
              lcd.print("prof a la clé");
              client.println("Un professeur possède la clé<br>");
            }
            else
            {
              client.println("Doing nothing<br>");
            }
            
            client.println("</body>\n</html>");
            client.println();
            break;
          }
        }
        if (c == '\n')
        {
          // you're starting a new line
          currentLineIsBlank = true;
        }
        else if (c != '\r')
        {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(50);

    // close the connection:
    Serial.println("close connection");
    client.stop();
    Serial.println("client disconnected");
  }
}

void printWifiStatus()
{
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  Serial.print("subnet mask: ");
  Serial.println(LWiFi.subnetMask());

  Serial.print("gateway IP: ");
  Serial.println(LWiFi.gatewayIP());

  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

