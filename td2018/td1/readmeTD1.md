# M4207 - TD1

*Bryan ADENOR, Christopher SELAMBAYE and Jean SALAUN-PENQUER*

## Aims
* Install and configure Arduino IDE
* Test the Blink code

## Our Work
- [x] Install and configuration of Arduino IDE 
- [x] Test the Blink Code

**Blink Code**
```c
void setup() {
  // put your setup code here, to run once:
  pinMode(13,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(13,HIGH);
  delay(100);
  digitalWrite(13,LOW);
  delay(100);
}

```
