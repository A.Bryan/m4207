# M4207 - TD2

*Bryan ADENOR, Christopher SELAMBAYE and Jean SALAUN-PENQUER*


## Exercices

* EX. 0: Serial Port
* EX. 1: Basic skills...
* EX. 2: GPS test
* EX. 3: WIFI test
* EX. 4: Data Storage: Write process
* EX. 5: Data Storage: Read process
* EX. 6: Web of Things
* EX. 7: Bonus

## Our Work

### EX. 0: Serial Port
**td2_ex0 Code**
```c
/* Message "Hello World!" and LED 13 blinking
NB: Don't forget to open the Serial Monitor on Tools Menu */

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(13, OUTPUT);
  Serial.begin(9600);                 // initialize baud rate for serial
  delay(1000);                        // wait for a second
  Serial.println("Hello World!");     //displays the message on the serial
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(13, HIGH);   // turn the LED 13 on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(13, LOW);    // turn the LED 13 off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

### EX. 1: Basic skills...
**td2_ex1 Code**
```c
/* Message "Hello World!" and LED 13 blinking
NB: Don't forget to open the Serial Monitor on Tools Menu */

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  Serial.begin(9600);                 // initialize baud rate for serial
}

int x = 0;
// the loop function runs over and over again forever
void loop() {
 Serial.print("The value of the counter is : ");
 Serial.println(x);
 delay(1000);
 x++;
}
```

###  EX. 2: GPS test
**td2_ex1 Code**