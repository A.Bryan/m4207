/* Message "Hello World!" and LED 13 blinking
NB: Don't forget to open the Serial Monitor on Tools Menu */

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(13, OUTPUT);
  Serial.begin(9600);                 // initialize baud rate for serial
  delay(1000);                        // wait for a second
  Serial.println("Hello World!");     //displays the message on the serial
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(13, HIGH);   // turn the LED 13 on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(13, LOW);    // turn the LED 13 off by making the voltage LOW
  delay(1000);                       // wait for a second
}
