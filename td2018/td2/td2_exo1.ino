/* Message "Hello World!" and LED 13 blinking
NB: Don't forget to open the Serial Monitor on Tools Menu */

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  Serial.begin(9600);                 // initialize baud rate for serial
}

int x = 0;
// the loop function runs over and over again forever
void loop() {
 Serial.print("The value of the counter is : ");
 Serial.println(x);
 delay(1000);
 x++;
}
