# Description of this repository

*Authors : Bryan Adénor, Christopher Selambaye and Jean Salaun-Penquer*

This repository contains Key Monitoring project. 
The directory "project2017" contains elements of a previous project that was forked 
from another repository. The fork was realized from https://gitlab.com/ulysse410/m4207

The directory "project2018" contains our project of Key Monitoring

The directory "td2018" contains some code for training during courses.