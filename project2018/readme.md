# M4207 - Project 2018

For this project, we have take the project of Lauret Ulysse (https://gitlab.com/ulysse410/m4207).

# The RTKeys Project

This project aims to improve the management of the keys of the RT Department. 
We have build a prototype composed of a LinkIt One and a Groove Starter Kit that we will describe more later.
This prototype would be miniaturized and attached to the key.

After that, we will use the WiFi of the department and a buzzer (included on the Groove Starter) that will 
make a noise.
We have also developped a web platform to register the users of the keys. With this same platform we could 
ask to the LinkIt to produce a sound if it was loosed and we will find it.

# Dependencies / Hardware / Software
Hardware prerequisites :
 - LinkIt One (https://www.seeedstudio.com/LinkIt-ONE-p-2017.html)
 - Groove Starter Kit Plus (http://wiki.seeedstudio.com/wiki/Grove_Starter_Kit_Plus_-_IoT_Edition)

Software prerequisites :
- Arduino IDE (download: https://www.arduino.cc/en/Main/Software) to create code for the LinkIt

# Usage
Mount your prototype with a LinkIt One Board and the Groove Kit. 
You must have a WiFi antenna and battery included with the LinkIt One package.
You also need a groove shield and the buzzer plugged on D3 (this is were the buzzer will work in our code). Two are included on the Groove Kit.

Launch your Arduino IDE and connect your LinkIt One to your computer. Ensure that you have the latest firmware on the board and the correct 
settings in the IDE for the LinkIt One.
Copy the final code on your IDE and set WiFi identifiers (WIFI_AP = login and WIFI_PASSWORD = password) then push it your to the LinkIt One.

Open the serial monitor (CTRL+SHIFT+M) and check the IP address of the board ("IP address" field). Type it on a browser.

You can now use the web interface to find the keys, borrow or release them. 


# Authors
Bryan ADENOR (@A.Bryan)
Jean SALAUN-PENQUER (@j.salaun)
Christopher SELAMBAYE (@c.selambaye)

# Todo
Improve the prototype by locating the key with WiFi APs. The prototype would gives you which AP is connected with him and you
have to know the area of this AP, so your searching zone will be smaller than the departement. 