#include <LBattery.h>

char buff[256]; //Store information about battery to be displayed

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  sprintf(buff,"Battery level = %d", LBattery.level()); //Store information on buff variable
  Serial.println(buff); //Print message in serial monitor
  
  //Check if battery is charging
  if(LBattery.isCharging() == 1){
    sprintf(buff,"Battery is charging");  //Store information on buff variable
    }
    else{
    sprintf(buff,"Battery is not charging");  //Store information on buff variable
    }
  Serial.println(buff); //Print message in serial monitor
 delay(1000); 
}

