#include <LTask.h>
#include <LWiFi.h>
#include <LWiFiServer.h>
#include <LWiFiClient.h>
#include <LBattery.h>

#define WIFI_AP "iPhoneJean"   //Choose the name of the SSID you want to use
#define WIFI_PASSWORD "azerty123"   //And select the password to be connected on this network
#define WIFI_AUTH LWIFI_WPA  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP according to your WiFi AP configuration
#define pinName = 3;

 String id_personne = "";

LWiFiServer server(80);

//Configure the pins numbers of the LED and the BUZZER 
int LED = 13; //To see on the Linkit one what step the code is
int BUZZER = 3; 

void setup()
{
  pinMode(LED, OUTPUT);
  LWiFi.begin();
  Serial.begin(115200);
  //Allowed the buzzer to work !
  pinMode(3, OUTPUT);
  digitalWrite(3, LOW);
  // keep retrying until connected to AP
  Serial.println("Connecting to AP");
  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD))) //When the linkit one doesn't connected to the network, blinded the LED 13
  {
    //blinked the LED 13
    digitalWrite(LED, HIGH);
    delay(100);
    digitalWrite(LED, LOW);
    delay(100);
    digitalWrite(LED, HIGH);
    delay(100);
    digitalWrite(LED, LOW);
    delay(600);
    
  }
  //The Server is starting
  digitalWrite(LED, HIGH);
  printWifiStatus();  //Give us information about the wifi connection (see the function at the end of the code
  Serial.println("Start Server");
  server.begin();
  Serial.println("Server Started");
  digitalWrite(LED, LOW);

}

int loopCount = 0;
boolean beepState = false;

void loop()
{
  // Configure variable to store the information we must to have 
  String str = "";
  String url = "";
  String id = "Personne n'a emprunté la clé";
  String rendre = "";
  int i;
  delay(500);
  loopCount++;
  LWiFiClient client = server.available();
   if (beepState == true){
      digitalWrite(BUZZER, HIGH);
      delay(400);
      digitalWrite(BUZZER, LOW);
      delay(2000);
    }
  if (client)
  {
    Serial.println("new client");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    while (client.connected())
    {
      if (client.available())   //Display on the serial monitor informations of the connection of the client
      {
        // we basically ignores client request, but wait for HTTP request end
        char c = client.read();
        //Serial.print("Value of c : ");  //Test : display the value of the c variable
        Serial.print(c);
        if(c != '\n')
          str += c;
        if(c == '\n')
        {
          Serial.print("Valur of str : ");
          Serial.println(str);
          //If we have a GET message
          if(str.startsWith("GET"))
          {
            //For the LED 13 (store the LED information chosen by the user to know if the LED turn on or turn off)
            if((str.substring(4, str.lastIndexOf("=")) == String("/?q")))//If user choose a state of the LED stock the state on url variable
            {
              url = str.substring(4, str.lastIndexOf(" "));
              Serial.print("URL:");
              Serial.print(url);
            }
            //For the ID (store the ID typed by the user who taken the key)
            if((str.substring(4, str.lastIndexOf("=")) == String("/?nom")))  //If user type his id stock id on id variable
            {
              id = str.substring(4, str.lastIndexOf(" "));
              Serial.print("ID:");
              Serial.println(id);
              id_personne = str.substring(10,str.lastIndexOf(" ")); //Take the user ID (that is after the "/?nom")
              Serial.print("id_personne : ");
              Serial.println(id_personne);  //Display the user ID who borrowed the key
            }
            //Store the information which allow to know if the user returned the key
            if((str.substring(4, str.lastIndexOf("=")) == String("/?rendre")))//If user choose a state of the LED stock the state on url variable
            {
              rendre = str.substring(4, str.lastIndexOf(" "));
              Serial.print("RENDRE:");
              Serial.println(rendre); //Display the ID of the user if he registred
              id_personne = "Personne n'a emprunte la cle";
            }
          }
          str = "";
        }

        if (c == '\n' && currentLineIsBlank)    //Part to modify to work the LED 13 and the register
        {
          Serial.println("send response");
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
          client.println();
          
          if(url != String("favicon.ico"))
          {
            //Website of the Linkit One
            Serial.println("Affiche test 1");
            client.println("<!DOCTYPE HTML>");
            client.println("<html>\n<head>\n<title>Nom de la Cle</title>\n</head>");
            IPAddress ip = LWiFi.localIP();
            //Old website we don't use it
            /*client.println("<body><h1>Bienvenue sur le site de la clé X</h1><h2>Commencez-donc par vous inscrire :</h2><center>");
            client.println("<form action='");
            client.println("' method='GET'>Tell your device what to do!<br><br><input type='radio' name='q' value='on'>Turn the LED on.<br><input type='radio' name='q' value='off'>Turn the LED off.<br><br><input type='submit' value='Do it!'></form>");
            client.println("' method='GET'><br><label for='nom'>Nom :</label><input type='text' name='nom' id='nom'/><br><input type='submit' value='S'enregistrer!'></form></br></br>");*/
            //Website that I modified
            client.println("<body> <center>");
            client.println("<?php error_reporting(0); ?>");
            client.println("<h2>Bienvenue sur le site de la cle X</h2></body></br>");
            client.println("<form action='' method='GET'>Tell your device what to do!<br><br><input type='radio' name='q' value='on'>Turn the LED and the Buzzer on.<br><input type='radio' name='q' value='off'>Turn the LED and the Buzzer off.<br><br><input type='submit' value='Do it!'></form><br><br>");
            client.println("<form action='' method='GET'>Enregistrez-vous.<br><br><label for='nom'>Nom :</label><input type='text' name='nom' id='nom'/><br></br><input type='submit' value='Register!'></form></br></br></form>");
            client.print("Afficher la personne qui a emprunte la cle.<br><br>Nom de l'ID : ");
            client.println(id_personne);
            client.println("</br></br></br>");
            client.println("<form action='' method='GET'>Rendre la cle<br><br><input type='submit' name='rendre' value='Rendre!'></form></br></br>"); //Si on appuie sur le bouton "Rendre!" on vide la variable id_personne
            url.toLowerCase();
            id.toLowerCase();
            //Test : display in the serial monitor the user ID
            Serial.print("Value of the id_personne : ");
            Serial.println(id_personne);
            Serial.print("Value of the id : ");
            Serial.println(id);
              
            //Turn on or turn off the LED 13 and display on the website the state of the LED
            if(url == String("/?q=on"))// Turn on the LED 13
            {
              digitalWrite(LED, HIGH);
              client.println("LED on<br>");
              Serial.println("LED ON !!");
              //Turn on the buzzer
              digitalWrite(BUZZER, HIGH);
              delay(400);
              digitalWrite(BUZZER, LOW);
              delay(2000);
              client.println("Buzzer on<br><br>");
              Serial.println("BUZZER ON !!");
              beepState = true;
            }
            else if(url == String("/?q=off"))//Turn off the LED 13
            {
              digitalWrite(LED, LOW);
              client.println("LED off<br>");
              Serial.println("LED OFF !!");
              //Turn off the buzzer
              digitalWrite(BUZZER, LOW);
              client.println("Buzzer off<br><br>");
              Serial.println("BUZZER OFF !!");
              beepState = false;
            }
            else
            {
              client.println("Doing nothing<br>");
            }  
            
            //ID Registred
            if(id != String(""))//If the user register
            {
              client.println("Identifiant registered<br>");
              Serial.println("ID work !!");
            }
            else
            {
              client.println("You don't put your ID<br>");  //If you didn't register
            }
            
            //Return the key
            if(rendre == String("/?rendre=Rendre%21"))//If the user return the key ("Rendre!" in URL it gives us "Rendre%21") 
            {              
              client.println("User returned the key<br>");
              Serial.println("Return the key!!");
            }
            
            //If the LinkitOne is on battery or not
            client.print("</br></br></br>Etat de la batterie : ");
            client.println(LBattery.level()); //display the battery level
            client.println("</br>");
            if(LBattery.isCharging() == 1){
              client.println("Battery is charging</br>");
            }
            else{
              client.println("Battery is not charging</br>");
            }
            client.println("<input type='button' onclick='window.location.reload(false)' value='Rafraichir'/>");
            client.println("</center></body>\n</html>");
            client.println();
            break;
          }
        }
        if (c == '\n')
        {
          // you're starting a new line
          currentLineIsBlank = true;
        }
        else if (c != '\r')
        {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(50);

    // close the connection:
    Serial.println("close connection");
    client.stop();
    Serial.println("client disconnected");
  }
}

void printWifiStatus()
{
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());

  // print your WiFi IP address:
  IPAddress ip = LWiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  Serial.print("subnet mask: ");
  Serial.println(LWiFi.subnetMask());

  Serial.print("gateway IP: ");
  Serial.println(LWiFi.gatewayIP());

  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
