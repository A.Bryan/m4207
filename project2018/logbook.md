# Logbook of the Project 2018

*Bryan ADENOR, Christopher SELAMBAYE and Jean SALAUN-PENQUER*


In this logbook, you will see how the Project 2018 will be manage.

At the begining of the course we have to create a project where we forked the project of Ulysse Lauret.
We organized the repository to make it easy to read and understand.

**02/02/2018 : Séance 1 (Jean and Bryan)**
Tasks : 
- [X] Read the work of the previous project (Jean et Bryan)
- [X] Modify the wifi.ino code  (Jean)
- [X] Modify the battery.ino code (Bryan)

**08/02/2018 : Séance 2 (Jean)**
Tasks : 
- [ ] Understand the wifi code
- [ ] Modify the wifi code

I was able to add to the web server and make the display part of the user ID on it and I try to understand clearly the wifi code.

**08/02/2018 : Séance 2 (Bryan)**
Tasks : 
- Test GPS function
- Test Sound Sensor

I have tested the GPS but it doesn't work on a room. Informations couldn't be received because of
the walls.
I am working on the sound sensor and I have to make a sound with the LinkIt One. After that, the
LinkIt One will make a sound when someone is searching the key.

**13/02/2018 : Séance 3 (Bryan)**
Tasks : 
- Understand and test wifi.ino code
- Create Sound Sensor code

I have read and tested the wifi.ino code. It worked on the Jean's iPhone but not on my Android...
I searched again how to create a buzzer sound but I failed. This task has to be done for the next seance.

**13/02/2018 : Séance 3 (Jean)**
Tasks : 
- [X] Can register on the linkit one via the web site
- [X] Can return the key
- [ ] Create a buzzer code

I have modify the wifi.ino code to allow at the user to register himself into the linkit one and to return the key.
At the end of the seance, I see how work the buzzer and how create the code to work it. For the next seance we need 
to know how work the battery code to prevent if the linkit is discharged (it can send us a message ?) 

**20/02/2018 : Seance 4 (Bryan)**
Tasks :
- [X] Makes a sound from the buzzer that alternates on/off
- [ ] Improve the readme.md of the project2018 directory
- [ ] Implement sound buzzer with wifi code

Todo
- Cf issue 6 

**23/02/2018 : Seance 4 (Jean)**
Tasks :
- [X] Implement sound buzzer with wifi code 
- [X] The final code was created (fusion of wifi code and buzzer code)
- [ ] Implement battery code in the wifi code

I was able to merge the wifi code with the buzzer code which gave the final code.
I tested the battery code and now I try to put it in the final code.
I modified the layout of the web server page to make it more understandable.

**28/02/2018 : Seance 5 (Jean)**
Tasks :
- [X] Comments in the final code
- [X] Test of the final code
- [ ] Improvement the buzzer in the final code

I finished the implementation of the code of the battery in the final code. I made the comments to help better understand the final code.
It would be necessary to improve the buzzer in the final code with the buzzerGroouve code

**28/02/2018 : Seance 5 (Bryan)
Tasks :
- [] Implement functions on the final code
- [] Debug session management

I have to implement functions on the final code to understand and debug it easily. Actually, the sessions are not managed very well...
For example, if someone have been connected and someone else would to connect, the server is unreachable (I think it's because the port 80 is busy).

After a discussion with Mr Turquay, we have to think about hand-over with APs. If we loose connection with an AP, we have to try another connection 
with another AP.

To do :
- [] AP Hand-over

